package com.lumanmed.pathinfo.camera.rich;

import io.loli.storage.redirect.RequestAuthFilter;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.http.util.HttpStatus;

import com.lumanmed.pathinfo.client.server.FileListener;

public class StartRequestAuthFilter implements RequestAuthFilter {
    private Camera camera;
    private Executor service = Executors.newSingleThreadExecutor();
    private volatile boolean running = false;

    @Override
    public void filter(final Request req, Response response) {
        if (req.getRequestURI().contains("start")) {
            if (!running) {
                service.execute(new Runnable() {
                    @Override
                    public void run() {
                        if (camera == null) {
                            try {
                                camera = new Camera();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                camera = null;
                                running = false;
                            }
                        }
                    }
                });
                try {
                    response.setStatus(HttpStatus.OK_200);
                    response.getWriter().write("run");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        } else if (req.getRequestURI().contains("gross")) {
            try {
                Runtime.getRuntime().exec("D:\\pis\\gross.bat");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (req.getRequestURI().contains("scan")) {
            try {
                Process process = Runtime
                        .getRuntime()
                        .exec("C:\\Program Files (x86)\\Luman\\LumanScanner\\lumanScanner.exe");
                File file = new FileListener(
                        "C:\\Users\\Public\\LumanData\\images\\scannedImages",
                        process).startListen();
                if (file != null) {
                    System.out.println("检测到新图片，准备上传");
                    MessageSender
                            .uploadWithPost(
                                    "http://98.194.38.12/:8080/pathinfo/image/richCamera/upload?type=originReportPost",
                                    file);

                }
                process.destroy();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (req.getRequestURI().endsWith("")) {
            
        }
    }
}
