package com.lumanmed.pathinfo.camera.rich;

import javax.imageio.ImageIO;
import javax.swing.*;

import com.lumanmed.pathinfo.camera.utils.PropertiesUtil;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ImagePanel extends JPanel {
	private static String uploadUrl;
	private BufferedImage image;
	private ImageListFrame jframe;
	private ListContainerPanel parent;

	private JButton save = new JButton("插入");
	private JButton remove = new JButton("删除");
	private Executor service = Executors.newCachedThreadPool();
	
	static {
		uploadUrl = PropertiesUtil.PROTOCOL + "://" + PropertiesUtil.IP + ":" +
				PropertiesUtil.PORT + PropertiesUtil.PATH;
	}

	public ImagePanel(final BufferedImage image, final ImageListFrame jframe,
			final ListContainerPanel parent) {
		this.image = image;
		this.jframe = jframe;
		this.parent = parent;
		this.setPreferredSize(new Dimension(160, 130));
		this.add(save);
		this.add(remove);
		save.setVisible(false);
		remove.setVisible(false);
		this.addMouseListener(new CustomMouseListener(Arrays.asList(save,
				remove)));
		save.addMouseListener(new CustomMouseListener(Arrays.asList(save,
				remove)));
		remove.addMouseListener(new CustomMouseListener(Arrays.asList(save,
				remove)));

		remove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				parent.getPanel().remove(remove.getParent());
				ImageListFrame.imageCount--;
				parent.getPanel()
						.setPreferredSize(
								new Dimension(
										320,
										ImageListFrame.imageCount % 2 == 1 ? (ImageListFrame.imageCount + 1) * 137 / 2
												: ImageListFrame.imageCount
														* (137 / 2)));
				revalidate();
				repaint();
				parent.repaint();
				parent.revalidate();
				jframe.revalidate();
				jframe.repaint();
			}

		});
		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					final File file = File.createTempFile(
							String.valueOf(System.nanoTime()), "png");
					if (file != null) {
						System.out.println("file is not null");
						ImageIO.write(image, "png", file);
						service.execute(new Runnable() {

							@Override
							public void run() {
								System.out.println("start send image");
								MessageSender.upload(uploadUrl, file);
							}
						});

					} else {
						System.out.println("Failed to create temp file");
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

		});
	}

	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), this);
	}
}

class CustomMouseListener extends MouseAdapter {

	public List<JButton> list;

	public CustomMouseListener(List<JButton> list) {
		super();
		this.list = list;
	}

	@Override
	public void mouseExited(MouseEvent e) {
		for (Component c : list) {
			c.setVisible(false);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		for (Component c : list) {
			c.setVisible(true);
		}
	}
}