package com.lumanmed.pathinfo.camera.rich;

import io.loli.storage.redirect.RedirectServer;

import java.io.IOException;

public class Server {
    private RedirectServer server = new RedirectServer();

    public Server() {
        try {
            server.port(28434).address("0.0.0.0")
                    .filter(new StartRequestAuthFilter()).start();
            while (true) {
                Thread.sleep(500);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Server();
    }
}
