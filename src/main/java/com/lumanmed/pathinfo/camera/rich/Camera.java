package com.lumanmed.pathinfo.camera.rich;


import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.awt.image.BufferedImage;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.UIManager;

import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.OpenCVFrameGrabber;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

/**
 * 
 * Use JavaCV/OpenCV to capture camera images
 * 
 * There are two functions in this demo: 1) show real-time camera images 2)
 * capture camera images by mouse-clicking anywhere in the JFrame, the jpg file
 * is saved in a hard-coded path.
 * 
 * @author ljs 2011-08-19
 * 
 */
public class Camera {
	public static String savedImageFile = "c:\\my.jpg";

	// timer for image capture animation
	static class TimerAction implements ActionListener {
		private Graphics2D g;
		private CanvasFrame canvasFrame;
		private int width, height;

		private int delta = 10;
		private int count = 0;

		private Timer timer;

		public void setTimer(Timer timer) {
			this.timer = timer;
		}

		public TimerAction(CanvasFrame canvasFrame) {
			this.g = (Graphics2D) canvasFrame.getCanvas().getGraphics();
			this.canvasFrame = canvasFrame;
			this.width = canvasFrame.getCanvas().getWidth();
			this.height = canvasFrame.getCanvas().getHeight();
		}

		public void actionPerformed(ActionEvent e) {
			int offset = delta * count;
			if (width - offset >= offset && height - offset >= offset) {
				g.drawRect(offset, offset, width - 2 * offset, height - 2
						* offset);
				canvasFrame.repaint();
				count++;
			} else {
				// when animation is done, reset count and stop timer.
				timer.stop();
				count = 0;
			}
		}
	}

	public Camera() throws Exception {
		// open camera source

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}
		OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
		grabber.start();

		// create a frame for real-time image display
		CanvasFrame canvasFrame = new CanvasFrame("Camera");
		canvasFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		 IplImage  image = grabber.grab();
		int width = image.width();
		int height = image.height();
		canvasFrame.setCanvasSize(width, height);
		canvasFrame.setResizable(false);
		// onscreen buffer for image capture
		final BufferedImage bImage = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D bGraphics = bImage.createGraphics();

		// animation timer
		TimerAction timerAction = new TimerAction(canvasFrame);
		final Timer timer = new Timer(10, timerAction);
		timerAction.setTimer(timer);
		final Queue<BufferedImage> que = new ArrayBlockingQueue<BufferedImage>(
				1);
		// click the frame to capture an image

		final ImageListFrame listFrame = new ImageListFrame(
				canvasFrame.getHeight(), canvasFrame.getWidth()
						+ canvasFrame.getX(), canvasFrame.getY());
		canvasFrame.addComponentListener(new ComponentAdapter() {
			public void componentMoved(ComponentEvent e) {
				listFrame.setLocation(e.getComponent().getX()
						+ e.getComponent().getWidth(), e.getComponent().getY());
			}
		});

		canvasFrame.getCanvas().addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				timer.start(); // start animation
				ImageListFrame.imageCount++;
				listFrame
						.getContainerPanel()
						.getPanel()
						.setPreferredSize(
								new Dimension(
										320,
										ImageListFrame.imageCount % 2 == 1 ? (ImageListFrame.imageCount + 1) * 137 / 2
												: ImageListFrame.imageCount
														* (137 / 2)));

				JPanel panel = new ImagePanel(que.peek(), listFrame, listFrame
						.getContainerPanel());

				listFrame.getContainerPanel().getPanel().add(panel);
				listFrame.getContainerPanel().repaint();
				listFrame.getContainerPanel().revalidate();
				listFrame.repaint();
				listFrame.revalidate();
				listFrame.getContainerPanel().scrollToBottom();
			}
		});

		canvasFrame.addWindowStateListener(new WindowStateListener() {
			public void windowStateChanged(WindowEvent state) {
				if (state.getNewState() == 1 || state.getNewState() == 7) {
					listFrame.setExtendedState(JFrame.ICONIFIED);
				} else if (state.getNewState() == 0) {
					listFrame.setExtendedState(JFrame.NORMAL);
				} else if (state.getNewState() == 6) {
				}
			}
		});

		canvasFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(final WindowEvent event) {
				ImageListFrame.imageCount = 0;
				listFrame.getContainerPanel().getPanel().removeAll();
				listFrame.getContainerPanel().removeAll();
				listFrame.removeAll();
			}
		});
		// real-time image display
		while (canvasFrame.isValid() && canvasFrame.isVisible()
				&& (image = grabber.grab()) != null) {
			if (!timer.isRunning()) { // when animation is on, pause real-time
										// display
				canvasFrame.showImage(image);
				// draw the onscreen image simutaneously
				que.poll();
//				byte[] imgBytes = new byte[image.image.length];
//				for(int i=0;i<imgBytes.length;i++){
//					imgBytes[i] = image.image[i];
//				}
				que.add(image.getBufferedImage());
				bGraphics.drawImage(image.getBufferedImage(), null, 0, 0);
			}
			Thread.sleep(50);
		}
		// release resources
		grabber.stop();
		canvasFrame.dispose();
		listFrame.dispose();
	}

	public static void main(String[] args) throws Exception {
		new Camera();
	}
}