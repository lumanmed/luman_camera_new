package com.lumanmed.pathinfo.camera.rich;

import javax.swing.*;

public class ListContainerPanel extends JScrollPane {
    private static JPanel jpanel = new JPanel();

    public ListContainerPanel() {
        super(jpanel);
        this.setLayout(new ScrollPaneLayout());
    }

    public JPanel getPanel() {
        return jpanel;
    }

    public void scrollToBottom() {
        JScrollBar sBar = this.getVerticalScrollBar(); // 得到了该JScrollBar
        sBar.setValue(sBar.getMaximum()); // 设置一个具体位置，value为具体的位置

    }
}
