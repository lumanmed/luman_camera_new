package com.lumanmed.pathinfo.camera.rich;

import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class MessageSender {
	private static CloseableHttpClient client = HttpClients.createDefault();

	public MessageSender() {
	}

	public static String upload(String url, File file) {
		String result = "";
		HttpPost post = new HttpPost(url);
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		System.out.println("encoding file");
		list.add(new BasicNameValuePair("base64", Base64Util.encode(file)));
		try {
			post.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
			HttpResponse response = client.execute(post);
			result = EntityUtils.toString(response.getEntity());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(result);
		return "";
	}

	public static void uploadWithPost(String url, File file) {

		try {
			HttpPost hp = new HttpPost(url);
			MultipartEntityBuilder multiPartEntityBuilder = MultipartEntityBuilder
					.create();
			multiPartEntityBuilder
					.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			// 可以直接addBinary
			multiPartEntityBuilder.addPart(
					"image",
					new FileBody(file, ContentType.create("image/png",
							Consts.UTF_8), file.getName()));
			multiPartEntityBuilder.setCharset(Consts.UTF_8);
			HttpResponse response;
			hp.setEntity(multiPartEntityBuilder.build());
			response = client.execute(hp);
			String result = null;
			result = EntityUtils.toString(response.getEntity());
			System.out.println(result);

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
