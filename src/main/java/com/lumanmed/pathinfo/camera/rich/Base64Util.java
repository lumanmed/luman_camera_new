package com.lumanmed.pathinfo.camera.rich;

import org.apache.commons.codec.binary.Base64;

import java.io.*;

public class Base64Util {

	public static String encode(File file) {
		String str = "";
		FileInputStream is = null;
		try {
			is = new FileInputStream(file);
			str = Base64.encodeBase64String(inputStreamToByte(is));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

	/**
	 * 将输入流转换成字节数组
	 * 
	 * @param is
	 *            需要转换的输入流
	 * @return 转换后的字节数组
	 * @throws IOException
	 *             当输入输出出现问题时抛出异常
	 */
	public static byte[] inputStreamToByte(InputStream is) throws IOException {
		ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
		byte[] b = new byte[4096];
		while (is.read(b) != -1) {
			bytestream.write(b);
		}
		byte data[] = bytestream.toByteArray();
		bytestream.close();
		return data;
	}

}