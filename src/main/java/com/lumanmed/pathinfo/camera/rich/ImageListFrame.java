package com.lumanmed.pathinfo.camera.rich;

import javax.swing.*;
import java.awt.*;

public class ImageListFrame extends JFrame {
	private static final long serialVersionUID = -7779436805175344765L;
	private ListContainerPanel container = new ListContainerPanel();
	public static volatile int imageCount = 0;

	public ImageListFrame(int height, int x, int y) {
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setPreferredSize(new Dimension(370, height));
		this.setSize(370, height);
		container.setPreferredSize(new Dimension(330, height));
		this.setLocation(x, y);
		this.setVisible(true);
		this.add(container);
		container.getPanel().setPreferredSize(
				new Dimension(170, imageCount * 150));
		
	}

	public ListContainerPanel getContainerPanel() {
		return container;
	}

	public static void main(String[] args) {
		new ImageListFrame(500, 200, 200);
	}

}
