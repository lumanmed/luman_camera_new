package com.lumanmed.pathinfo.client.server;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import java.util.zip.CRC32;

public class FileListener {
    private File folder = null;
    private Process pro = null;
    private static Logger logger = Logger.getLogger(FileListener.class
            .getName());

    public FileListener(String path, Process process) {
        folder = new File(path);
        pro = process;
    }

    /**
     * 获取新添加的文件，会一直阻塞30秒，如果没有新文件，则会弹出框
     * 
     * @return
     */

    public File startListen() {
        Vector<File> imageQueue = new Vector<File>();
        Vector<File> totalQueue = new Vector<File>();

        List<String> files = new ArrayList<String>();
        double timeout = 0;
        boolean cont = true;
        File result = null;

        int count = 0;
        Z: while (timeout < 40 && cont) {
            count++;
            // Z: while (timeout < 1 && cont) {
            File[] images = folder.listFiles(new FileFilter() {

                @Override
                public boolean accept(File file) {
                    if (file.isFile()
                            && (file.getName().endsWith(".jpg")
                                    || file.getName().endsWith(".JPG")
                                    || file.getName().endsWith(".JPEG")
                                    || file.getName().endsWith(".jpeg")
                                    || file.getName().endsWith(".png") || file
                                    .getName().endsWith(".PNG"))) {
                        return true;
                    }
                    return false;
                }
            });
            for (File i : images) {
                boolean found = false;
                for (File j : totalQueue) {
                    if (i.getAbsolutePath().equals(j.getAbsolutePath())) {
                        found = true;
                    }
                }

                if (!found) {
                    System.out.println(String.format(
                            "Found image %s, added to queue.",
                            i.getAbsolutePath()));
                    totalQueue.add(i);

                    // 是否继续
                    if (count > 1) {
                        // 需要等待以便文件大小不再变了
                        System.out
                                .println("Waiting for image size not changing");
                        boolean connn = true;
                        long lastModified = crc32(i);
                        System.out.println("CRC32 is " + lastModified);
                        int sameCount = 0;
                        while (true && connn) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            File newFile = new File(i.getAbsolutePath());
                            long crc = crc32(newFile);
                            if (lastModified == crc) {
                                System.out.println("CRC32 not changed");
                                sameCount++;
                                if (sameCount > 2) {
                                    break;
                                }
                            } else {
                                System.out.println("CRC32 changed");
                                lastModified = crc;
                            }

                        }
                        result = i;
                        System.out.println("Prepare to upload");
                        break Z;
                    }

                } else {
                }
            }

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
            timeout += 0.5;
            if (timeout >= 40) {
                int j = JOptionPane.showConfirmDialog(null,
                        "没有检测到扫描的新图片，是否继续等待？");
                cont = (j == 1);
            }
        }
        return result;

    }

    public long crc32(File file) {
        CRC32 crc = new CRC32();
        try {
            InputStream is = new BufferedInputStream(new FileInputStream(file));
            byte[] bytes = new byte[1024];
            while (is.read(bytes) != -1) {
                crc.update(bytes);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return crc.getValue();

    }

}
